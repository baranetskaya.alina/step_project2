Описание проекта:
проект лендинг-страницы програмного продукта для веб-разработки Forkio. 

Подготовили:
задание No1 julia ruzmetova (@optimistic376).
заданиеNo2Alina Baranetska (@baranetskaya.alina).

Создано с помощью:
1.система управления версиями Git. 
2.система сборки и задач Gulp.
3.HTML, CSS, JS  + SASS.

Инструкция по сборке:
1.git clone https://gitlab.com/optimistic376/step_project2.
2.в папке с проектом вызываем терминал и прописываем npm install.
